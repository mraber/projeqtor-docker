# vim:set ft=dockerfile:

ARG REGISTRY=docker.io

# Local: --build-arg PJQ_ARCHIVE_URL=./assets/projeqtorV6.6.6.zip
ARG PJQ_VERSION=12.0.1
ARG PJQ_ARCHIVE_URL=https://sourceforge.net/projects/projectorria/files/projeqtorV${PJQ_VERSION}.zip/download
ARG PJQ_ARCHIVE_NAME=projeqtorV${PJQ_VERSION}.zip
ARG PJQ_ARCHIVE_DIR=projeqtor
ARG PJQ_EXTRACT_DIR=/export

# ==============================================================================
# == DOWNLOAD IMAGE
# ==============================================================================
FROM ${REGISTRY}/ubuntu:latest AS archive

ARG PJQ_VERSION
ARG PJQ_ARCHIVE_URL
ARG PJQ_ARCHIVE_NAME
ARG PJQ_ARCHIVE_DIR
ARG PJQ_EXTRACT_DIR

# OS: packages (install)
# -----------------------------------------------
  RUN echo 'Update OS and install packages' \
  && apt-get -yqq update \
  && apt-get -yqqu upgrade \
  && apt-get -yqq install \
    curl \
    unzip \
  && apt-get -yqq clean \
  && rm -rf /var/lib/apt/lists/*

# Projeqtor: download distribution
# -----------------------------------------------
# Note: Works because the archive is a 'zip' file so it will not be extracted (local source)!
# Not the most efficient (a layer with 50MB) but allows to test locally without downloading
# the same archive over and over
# Another way would be to start a local web server ('python3 -m http.server')
ADD ${PJQ_ARCHIVE_URL} /tmp/${PJQ_ARCHIVE_NAME}

RUN mkdir ${PJQ_EXTRACT_DIR} \
  && unzip -q /tmp/${PJQ_ARCHIVE_NAME} -d ${PJQ_EXTRACT_DIR} \
  && rm -f /tmp/${PJQ_ARCHIVE_NAME}

# ==============================================================================
# == FINAL IMAGE
# ==============================================================================
FROM ${REGISTRY}/php:8.3.17-apache AS build

ARG PJQ_ARCHIVE_DIR
ARG PJQ_EXTRACT_DIR

# OS: packages (install)
# -----------------------------------------------
RUN echo 'Update OS and install packages' \
  && apt-get -yqq update \
  && apt-get -yqqu upgrade \
  && apt-get -yqq install \
    apt-utils \
    unzip \
  && apt-get -yqq clean \
  && rm -rf /var/lib/apt/lists/*

# OS: config
# -----------------------------------------------
RUN echo 'Configure OS: nothing to do'

# PHP: plugins
#
# FIXME '-dev' library are not required at runtime
# FIXME pin packages version
# -----------------------------------------------
RUN echo "Install PHP extentions..." \
  && echo 'PHP => GD' \
  && apt-get -yqq update \
  && apt-get -yqq install \
    libfreetype6 \
    libfreetype6-dev \
    libjpeg62-turbo \
    libjpeg62-turbo-dev \
    libpng-dev \
    libxpm-dev \
    libxpm4 \
    libldap-common \
    libldap2-dev \
  && apt-get -yqq clean \
  && rm -rf /var/lib/apt/lists/* \
  && docker-php-ext-configure gd --with-freetype --with-jpeg \
  && docker-php-ext-install -j$(nproc) gd \
  && echo 'PHP => (PDO_)PGSQL' \
  && apt-get -yqq update \
  && apt-get -yqq install libpq5 \
    libpq-dev \
  && apt-get -yqq clean \
  && rm -rf /var/lib/apt/lists/* \
  && docker-php-ext-install -j$(nproc) pgsql \
  && docker-php-ext-install -j$(nproc) pdo_pgsql \
  && echo 'PHP => (PDO_)MYSQL' \
  && docker-php-ext-install -j$(nproc) mysqli \
  && docker-php-ext-install -j$(nproc) pdo_mysql \
  && echo 'PHP => LDAP' \
  && docker-php-ext-configure ldap \
  && docker-php-ext-install -j$(nproc) ldap \
  && echo 'PHP => ZIP' \
  && apt-get -yqq update \
  && apt-get -yqq install libzip-dev zip \
  && apt-get -yqq clean \
  && rm -rf /var/lib/apt/lists/* \
  && docker-php-ext-install -j$(nproc) zip

# PROJEQTOR: install
# -----------------------------------------------
COPY ./config/start.sh /start.sh

RUN chmod 755 /start.sh \
  && mkdir --mode=770 -p /var/data/projeqtor/attachments \
  && mkdir --mode=770 -p /var/data/projeqtor/documents \
  && mkdir --mode=770 -p /var/data/projeqtor/logs \
  && chown -R www-data:www-data /var/data/projeqtor

COPY --from=archive --chown=www-data:www-data ${PJQ_EXTRACT_DIR}/${PJQ_ARCHIVE_DIR}/ /var/www/html

# PROJEQTOR: configuration
# -----------------------------------------------
COPY --chown=www-data:www-data ./config/parametersLocation.php /var/www/html/tool/
COPY --chown=www-data:www-data ./config/parameters.php /var/data/projeqtor/config/parameters.php

ENV PJQ_DB_TYPE='mysql' \
  PJQ_DB_HOST='127.0.0.1' \
  PJQ_DB_PORT='3306' \
  PJQ_DB_USER='root' \
  PJQ_DB_PASSWORD='super/password' \
  PJQ_DB_NAME='projeqtor' \
  PJQ_DB_DISPLAY_NAME='Projeqtor' \
  PJQ_DB_PREFIX='' \
  PJQ_DB_COLLATION='' \
#  PJQ_SSL_KEY='' \
#  PJQ_SSL_CERT='' \
#  PJQ_SSL_CA='' \
#  PJQ_LDAP_ALLOW_LOGIN='false' \
#  PJQ_LDAP_BASE_DN='dc=mydomain,dc=com' \
#  PJQ_LDAP_HOST='ldap://localhost:389' \
#  PJQ_LDAP_PORT='' \
#  PJQ_LDAP_VERSION='3' \
#  PJQ_LDAP_SEARCH_USER='cn=Manager,dc=mydomain,dc=com' \
#  PJQ_LDAP_SEARCH_PASSWORD='secret' \
#  PJQ_LDAP_USER_FILTER='uid=%USERNAME%' \
#  PJQ_SMTP_SERVER='localhost' \
#  PJQ_SMTP_PORT='25' \
#  PJQ_SENDMAIL_PATH='' \
  PJQ_PASSWORD_MIN_LENGTH='5' \
  PJQ_DEFAULT_LOCALE='en' \
  PJQ_DEFAULT_TIMEZONE='Europe/Paris' \
  PJQ_CURRENCY='€' \
  PJQ_CURRENCY_POSITION='after' \
  PJQ_DIR_ATTACHMENTS='/var/data/projeqtor/attachments' \
  PJQ_DIR_DOCUMENTS='/var/data/projeqtor/documents' \
  PJQ_ATTACHMENT_MAX_SIZE='10485760' \
  PJQ_ATTACHMENT_MAX_SIZE_EMAIL='2097152' \
  PJQ_REPORT_TEMP_DIR='../files/report/' \
  PJQ_MEMORY_LIMIT_PDF='512' \
  PJQ_LOG_FILE='/var/data/projeqtor/logs/projeqtor_$${date}.log' \
  PJQ_LOG_LEVEL='2'

# PHP: configuration
# -----------------------------------------------
# --build-arg PHP_CONFIG_MODE=development
ARG PJQ_PHP_CONFIG_MODE=production

RUN cp "${PHP_INI_DIR}/php.ini-${PJQ_PHP_CONFIG_MODE}" "${PHP_INI_DIR}/php.ini"

ENV PJQ_PHP_MAX_INPUT_VARS=2000 \
  PJQ_PHP_REQUEST_TERMINATE_TIMEOUT=0 \
  PJQ_PHP_MAX_EXECUTION_TIME=30 \
  PJQ_PHP_MEMORY_LIMIT=512M \
  PJQ_PHP_MAX_UPLOAD_SIZE=512M

COPY ./config/99-php-projeqtor.ini "${PHP_INI_DIR}/conf.d/"

# Start container
# -----------------------------------------------
CMD ["/start.sh"]
