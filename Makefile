SHELL=/bin/bash

include $(PWD)/env.prod

IMG_LATEST=projeqtor:latest
IMG_TAG=projeqtor:$(PRJ_TAG)
CFG_LOCAL_STORAGE=./tests/test-local-dirs.yml
LOCAL_DIRS=$(shell yq '.volumes.[] | .driver_opts | .device' $(CFG_LOCAL_STORAGE))

.DEFAULT_GOAL:=help

.PHONY: help
help:                                            ## Display this help message
	$(info Helper to build and test Projeqtor images.)
	@awk \
		'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' \
		$(MAKEFILE_LIST)

.PNONY: build-local
build-local:                                     ## Build a new image (tags: 'latest' and '<version_env_prod>')
	docker build --no-cache -t $(IMG_LATEST) -t $(IMG_TAG) .

.PNONY: test-init
test-init:                                       ## Initialize env for local testing
	mkdir -p $(LOCAL_DIRS)

clean:                                           ## Remove stopped containers
	./hacks/clean.sh containers

clean-all: clean                                 ## Remove stopped containers and purge data
	./hacks/clean.sh volumes
	sudo rm -fr $(LOCAL_DIRS)

test-local: test-init                            ## Start the app (latest)
	docker compose -f compose.yaml -f $(CFG_LOCAL_STORAGE) up -V

test-local-ldap: test-init                       ## Start the app (latest, LDAP server)
	docker compose -f ./tests/compose-ldap.yaml -f $(CFG_LOCAL_STORAGE) up -V

test-prod: test-init                             ## Start the app (from Docker.io)
	docker compose -f compose.yaml -f $(CFG_LOCAL_STORAGE) --env-file ./env.prod up -V
