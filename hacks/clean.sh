#!/bin/bash

# =================================
# Shell configuration
# =================================
set -o nounset  # ("-u") Fail when using unset var
set -o pipefail # (-)    Do not mask errors in pipeline

# ================
# = FUNCTIONS
# ================

function help() {
  echo "Usage: clean.sh help | containers | volumes"
}

function clean_unused_containers() {
  local rep=0

  docker ps -a --filter=name=projeqtor --filter=status=exited --quiet | xargs -r docker rm
  rep=$?
  return $rep
}

function clean_volumes() {
  local rep=0

  docker volume ls -q --filter=name=projeqtor | xargs -r docker volume rm
  rep=$?
  return $rep
}

# ================
# = MAIN
# ================

Exit_Code=0
case ${1:-help} in
  help)
    help ; Exit_Code=$? ;;
  containers)
    clean_unused_containers ; Exit_Code=$? ;;
  volumes)
    clean_volumes ; Exit_Code=$? ;;
esac
exit $Exit_Code
