# Projeqtor

Deploy [Projeqtor](https://www.projeqtor.org/fr/) with Docker and Docker Compose.

> **This image is provided on a _"best effort"_ basis and is by no means provided
> nor supported by the Projeqtor team.**

## Introduction

### Testing

A [Vagrantfile](https://www.vagrantup.com) is provided to ease testing:

```bash
vagrant up
```

It will provisionned a VM (Ubuntu), install Docker and Docker Compose,
build the image and start Projeqtor.

Application will be available at <http://localhost:8080>

### Prerequisites

Docker (28+) and Docker compose (2.22+) must be available.

### Base Docker images

- <https://hub.docker.com/_/php> (`8.3.x-apache`)
- <https://hub.docker.com/_/mariadb> (`11.4.x-noble`)

### Use image

Images are available on the [Docker Hub](https://hub.docker.com/r/mraber/projeqtor):

```bash
docker pull mraber/projeqtor:12.0.1-{REV}
```

## Build & Test

### Configuration

The following arguments may be use to customize the build (`--build-arg`):

|         Name          |                                     Default value                                     |                 Description                 |
| :-------------------: | :-----------------------------------------------------------------------------------: | :-----------------------------------------: |
|      `REGISTRY`       |                                      `docker.io`                                      |       Where to search for base images       |
|      `PROXY_URL`      |                                          ``                                           |                 HTTP proxy                  |
|     `PJQ_VERSION`     |                                        `12.0.1`                                       |              Projeqtor version              |
|   `PJQ_ARCHIVE_URL`   | `https://freefr.dl.sourceforge.net/project/projectorria/projeqtorV${PJQ_VERSION}.zip` |          Download URL or **path**           |
|  `PJQ_ARCHIVE_NAME`   |                            `projeqtorV${PJQ_VERSION}.zip`                             |                Archive name                 |
|   `PJQ_EXTRACT_DIR`   |                                      `projeqtor`                                      | Resulting directory after arhive extraction |
| `PJQ_PHP_CONFIG_MODE` |                                     `production`                                      |          PHP config file template           |

### Build

To build the image yourself:

```bash
docker build --no-cache -t projeqtor:latest ./
```

### Test

To test your [build](#build), run:

```bash
docker compose up -d
```

## Deploy

### Breaking changes

#### `v9` to `v10`

- Projeqtor has:
  - change database encoding scheme (see [here](https://www.projeqtor.org/en/product-en/downloads))
- this project has:
  - Renamed database
  - Set different database passwords for 'root' and 'app'
  - Renamed Docker volumes
  - Renamed Docker volumes directories ('production deployment')

Migrating from `v9` to `v10` requires to:

- Stop the service
- Do a full backup of Projeqtor:
  - Database
  - Documents & Attachments directories
- Delete the service and the volumes
- Start a fresh instance
- Import data:
  - Database
  - Documents & attachments directories
- Test

**CAUTION**: It is strongly advised to test the procedure on a sandbox environment before doing it on a real data!!

### Configuration

The following environment variables should be added to the `compose.yaml` file:

|                Name                 |                   Default value                   | Description |
| :---------------------------------: | :-----------------------------------------------: | :---------: |
|            `PJQ_DB_TYPE`            |                      'mysql'                      |    Desc.    |
|            `PJQ_DB_HOST`            |                    '127.0.0.1'                    |    Desc.    |
|            `PJQ_DB_PORT`            |                      '3306'                       |    Desc.    |
|            `PJQ_DB_USER`            |                      'root'                       |    Desc.    |
|          `PJQ_DB_PASSWORD`          |                 'super/password'                  |    Desc.    |
|            `PJQ_DB_NAME`            |                    'projeqtor'                    |    Desc.    |
|        `PJQ_DB_DISPLAY_NAME`        |                    'Projeqtor'                    |    Desc.    |
|           `PJQ_DB_PREFIX`           |                        ''                         |    Desc.    |
|         `PJQ_DB_COLLATION`          |                        ''                         |    Desc.    |
|            `PJQ_SSL_KEY`            |                      _unset_                      |    Desc.    |
|           `PJQ_SSL_CERT`            |                      _unset_                      |    Desc.    |
|            `PJQ_SSL_CA`             |                      _unset_                      |    Desc.    |
|       `PJQ_LDAP_ALLOW_LOGIN`        |                      _unset_                      |    Desc.    |
|         `PJQ_LDAP_BASE_DN`          |                      _unset_                      |    Desc.    |
|           `PJQ_LDAP_HOST`           |                      _unset_                      |    Desc.    |
|           `PJQ_LDAP_PORT`           |                      _unset_                      |    Desc.    |
|         `PJQ_LDAP_VERSION`          |                      _unset_                      |    Desc.    |
|       `PJQ_LDAP_SEARCH_USER`        |                      _unset_                      |    Desc.    |
|     `PJQ_LDAP_SEARCH_PASSWORD`      |                      _unset_                      |    Desc.    |
|       `PJQ_LDAP_USER_FILTER`        |                      _unset_                      |    Desc.    |
|          `PJQ_SMTP_SERVER`          |                      _unset_                      |    Desc.    |
|           `PJQ_SMTP_PORT`           |                      _unset_                      |    Desc.    |
|         `PJQ_SENDMAIL_PATH`         |                      _unset_                      |    Desc.    |
|      `PJQ_PASSWORD_MIN_LENGTH`      |                        '5'                        |    Desc.    |
|        `PJQ_DEFAULT_LOCALE`         |                       'en'                        |    Desc.    |
|       `PJQ_DEFAULT_TIMEZONE`        |                  'Europe/Paris'                   |    Desc.    |
|           `PJQ_CURRENCY`            |                        '€'                        |    Desc.    |
|       `PJQ_CURRENCY_POSITION`       |                      'after'                      |    Desc.    |
|        `PJQ_DIR_ATTACHMENTS`        |         '/var/data/projeqtor/attachments'         |    Desc.    |
|         `PJQ_DIR_DOCUMENTS`         |          '/var/data/projeqtor/documents'          |    Desc.    |
|      `PJQ_ATTACHMENT_MAX_SIZE`      |                    '10485760'                     |    Desc.    |
|   `PJQ_ATTACHMENT_MAX_SIZE_EMAIL`   |                     '2097152'                     |    Desc.    |
|        `PJQ_REPORT_TEMP_DIR`        |                '../files/report/'                 |    Desc.    |
|       `PJQ_MEMORY_LIMIT_PDF`        |                       '512'                       |    Desc.    |
|           `PJQ_LOG_FILE`            | '/var/data/projeqtor/logs/projeqtor_$${date}.log' |    Desc.    |
|           `PJQ_LOG_LEVEL`           |                        '2'                        |    Desc.    |
|      `PJQ_PHP_MAX_INPUT_VARS`       |                      `2000`                       |    Desc.    |
| `PJQ_PHP_REQUEST_TERMINATE_TIMEOUT` |                        `0`                        |    Desc.    |
|    `PJQ_PHP_MAX_EXECUTION_TIME`     |                       `30`                        |    Desc.    |
|       `PJQ_PHP_MEMORY_LIMIT`        |                      `512M`                       |    Desc.    |
|      `PJQ_PHP_MAX_UPLOAD_SIZE`      |                      `512M`                       |    Desc.    |

#### LDAP

LDAP is only supported from version **`11.3.0-2`** of this image (before that, the `ldap` PHP module wasn't installed).

**WARNING:** Since PHP 8.2, you must use the URI syntax to configure host and port:

```shell
PJQ_LDAP_HOST=ldap://<SERVER>:<PORT>
```

The `PJQ_LDAP_PORT` is not used anymore.

> An _example_ of Docker Compose with a LDAP server is available in the `tests/`
> directory.

##### Diagnose

Some sample commands that may be useful:

- `ldapsearch -x -W -H ldap://openldap:1389 -b "dc=example,dc=org" -D "cn=admin,dc=example,dc=org" -s sub`

### Introduction

The provided `compose.yaml` file will start an instance of MariaDB and an instance of Projeqtor.

- MariaDB:
  - Root access:
    - Default password: `veryComplicatedRo2tPassword*2022` (**do NOT keep this one!**)
    - Only allowed from `localhost`
  - Projeqtor app:
    - database: `projeqtor-db`
    - user: `projeqtor-user`
    - password: `veryComplicatedAppsPassword*2022` (**do NOT keep this one!**)
  - Misc:
    - Auto-upgrade is enabled (`mariadb-upgrade` will be run before the server starts if required)

Application is available at <http://localhost>.

Data are stored in multiple Docker volumes:

- `prj_db_data`: MariaDB database
- `prj_attachments`: Projeqtor attachments
- `prj_documents`: Projeqtor documents
- `prj_logs`: Projeqtor logs

### Getting Started

To download and start the latest available version from [Docker Hub](https://hub.docker.com/r/mraber/projeqtor),
run:

```bash
docker compose --env-file ./env.prod up -d
```

_Note: Add `-V` option if you do not want to reuse the data from a previous start._

### For production

In production, it's highly recommanded to use dedicated directories:

```bash
sudo mkdir -p /var/data/projeqtor-v10/{database,logs,attachments,documents}

docker compose --env-file ./env.prod -f compose.yaml -f prod-local-dirs.yml up -d
```

> **warning "v10: Breaking changes"**
>
> - Directories have been renamed between `v9.x` and `v10.x`.
> - Volumes have been renamed

## Other

### Stop Projeqtor

```bash
docker compose stop
```

### Remove Projeqtor

**WARN: all data will be lost!!**

```bash
docker compose down -v
# If using dedicated directories:
sudo rm -fr /var/data/projeqtor-v10/{database,logs,attachments,documents}
```

## Backup & Restore

### Database

#### Export an existing DB

TODO

#### Import a backup

TODO

### Projeqtor files

TODO
