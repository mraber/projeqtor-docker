#!/bin/bash
set -x
set -o pipefail

echo "=== Load Apache envvars ==="
source ${APACHE_ENVVARS}

echo "=== Set permissions on volumes ==="
chown -R ${APACHE_RUN_USER}:${APACHE_RUN_GROUP} ${PJQ_DIR_ATTACHMENTS}
chown -R ${APACHE_RUN_USER}:${APACHE_RUN_GROUP} ${PJQ_DIR_DOCUMENTS}
chown -R ${APACHE_RUN_USER}:${APACHE_RUN_GROUP} $(dirname ${PJQ_LOG_FILE})

echo "=== Starting Apache2 (foreground) ==="
apache2-foreground
