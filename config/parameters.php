<?php
// -- DATABASE -----------------------------------------------------------------
$paramDbType              = getenv('PJQ_DB_TYPE');
$paramDbHost              = getenv('PJQ_DB_HOST');
$paramDbPort              = getenv('PJQ_DB_PORT');
$paramDbUser              = getenv('PJQ_DB_USER');
$paramDbPassword          = getenv('PJQ_DB_PASSWORD');
$paramDbName              = getenv('PJQ_DB_NAME');
$paramDbDisplayName       = getenv('PJQ_DB_DISPLAY_NAME');
$paramDbPrefix            = getenv('PJQ_DB_PREFIX');
$paramDbCollation         = getenv('PJQ_DB_COLLATION');
// -- TLS ----------------------------------------------------------------------
if (getenv('PJQ_SSL_KEY')) {
  $SslKey                   = getenv('PJQ_SSL_KEY');
}
if (getenv('PJQ_SSL_CERT')) {
  $SslCert                  = getenv('PJQ_SSL_CERT');
}
if (getenv('PJQ_SSL_CA')) {
  $SslCa                    = getenv('PJQ_SSL_CA');
}
// -- LDAP ---------------------------------------------------------------------
if (getenv('PJQ_LDAP_ALLOW_LOGIN')) {
  $paramLdap_allow_login    = getenv('PJQ_LDAP_ALLOW_LOGIN');
}
if (getenv('PJQ_LDAP_BASE_DN')) {
  $paramLdap_base_dn        = getenv('PJQ_LDAP_BASE_DN');
}
if (getenv('PJQ_LDAP_HOST')) {
  $paramLdap_host           = getenv('PJQ_LDAP_HOST');
}
if (getenv('PJQ_LDAP_PORT')) {
  $paramLdap_port           = getenv('PJQ_LDAP_PORT');
}
if (getenv('PJQ_LDAP_VERSION')) {
  $paramLdap_version        = getenv('PJQ_LDAP_VERSION');
}
if (getenv('PJQ_LDAP_SEARCH_USER')) {
  $paramLdap_search_user    = getenv('PJQ_LDAP_SEARCH_USER');
}
if (getenv('PJQ_LDAP_SEARCH_PASSWORD')) {
  $paramLdap_search_pass    = getenv('PJQ_LDAP_SEARCH_PASSWORD');
}
if (getenv('PJQ_LDAP_USER_FILTER')) {
  $paramLdap_user_filter    = getenv('PJQ_LDAP_USER_FILTER');
}
// -- SMTP ---------------------------------------------------------------------
if (getenv('PJQ_SMTP_SERVER')) {
  $paramMailSmtpServer      = getenv('PJQ_SMTP_SERVER');
}
if (getenv('PJQ_SMTP_PORT')) {
  $paramMailSmtpPort        = getenv('PJQ_SMTP_PORT');
}
if (getenv('PJQ_SENDMAIL_PATH')) {
  $paramMailSendmailPath    = getenv('PJQ_SENDMAIL_PATH');
}
// -- PROJEQTOR: CONFIG --------------------------------------------------------
$paramPasswordMinLength   = getenv('PJQ_PASSWORD_MIN_LENGTH');
$paramDefaultLocale       = getenv('PJQ_DEFAULT_LOCALE');
$paramDefaultTimezone     = getenv('PJQ_DEFAULT_TIMEZONE');
$currency                 = getenv('PJQ_CURRENCY');
$currencyPosition         = getenv('PJQ_CURRENCY_POSITION');
// -- PROJEQTOR: STORAGE -------------------------------------------------------
$paramAttachmentDirectory = getenv('PJQ_DIR_ATTACHMENTS');
$documentRoot             = getenv('PJQ_DIR_DOCUMENTS');
$paramAttachmentMaxSize   = getenv('PJQ_ATTACHMENT_MAX_SIZE');
$AttachmentMaxSizeMail    = getenv('PJQ_ATTACHMENT_MAX_SIZE_EMAIL');
// -- PROJEQTOR: REPORTS -------------------------------------------------------
$paramReportTempDirectory = getenv('PJQ_REPORT_TEMP_DIR');
$paramMemoryLimitForPDF   = getenv('PJQ_MEMORY_LIMIT_PDF');
// -- PROJEQTOR: LOGS ----------------------------------------------------------
$logFile                  = getenv('PJQ_LOG_FILE');
$logLevel                 = getenv('PJQ_LOG_LEVEL');
// -- INTERNALS ----------------------------------------------------------------
# Mandatory (else login fails due to invalid characters!)
$enforceUTF8='1';
?>
