#!/bin/bash

set -o errexit  # Exit script at 1st error
set -o nounset  # Fail when using unset var
set -o pipefail # Do not mask errors in pipeline
#set -o xtrace   # Debug: print statements before execution

cat <<_EOF > /dev/stdout
  +-------------------------------------------------------------------+
  | BOOTSTRAP SCRIPT FOR INSTALLING PROJEQTOR ON VAGRANT / UBUNTU VM  |
  +-------------------------------------------------------------------+
  | The following will be done:                                       |
  |  * install Docker                                                 |
  |  * install Docker Compose                                         |
  |  * Build Projeqtor image                                          |
  |  * Start apps with Docker Compose                                 |
  +-------------------------------------------------------------------+
_EOF

# Prerequisites
echo "== Install prerequisites..."
apt-get -yqq update
apt-get -yqq install ca-certificates curl gnupg lsb-release

# Docker repository
echo "== Add Docker repository..."
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "${UBUNTU_CODENAME:-$VERSION_CODENAME}") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Installation
echo "== Install Docker..."
apt-get -yqq update
apt-get -yqq install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Test
echo "== Test Docker installation..."
docker run hello-world

# Docker Compose
echo "== Install Docker Compose..."
echo "----> Now part of Docker CLI. Use 'docker compose' instead of 'docker-compose'"

# Test
echo "== Docker Compose: $(docker compose version)"

# PROJEQTOR
echo "== Build Projeqtor image..."
cd /vagrant
docker build -t projeqtor:latest . > docker-build.log

echo "== Start Apps..."
docker compose --env-file ./vagrant/env.vagrant up -d

cat <<_EOF > /dev/stdout
  +-------------------------------------------------------------------+
  | BOOTSTRAP: DONE. ENJOY!!                                          |
  |                                                                   |
  | Projeqtor URL: http://localhost:8080                              |
  +-------------------------------------------------------------------+
_EOF

exit 0
